import UnaryOperations from './calc_un_operations.js'

function append_number($root, digit){
  return () => {
    if($root.input == null || $root.overwrite){
      $root.input = digit
    } else {
      $root.input += digit
    }

    $root.overwrite = false
  }
}

function enter($root){
  if(!isNaN($root.input_value)){
    $root.stack.push($root.input_value)
    $root.overwrite = true
  }
}

function bin_operation($root, callback){
  return () => {
    enter($root)

    $root.input_value = callback($root.stack.pop(), $root.stack.pop())
  }
}

function un_operation($root, func){
  return () => {
      let value = $root.last_value

      if($root.input != null){
        value = $root.input_value
      }

      $root.last_value = UnaryOperations[func](value)

      $root.input = null

      $root.operation = "equals"
  }
}

const noop = Function.prototype

export default {
  switch_sign($root){
    return () => {
      if($root.input != null){
        $root.input_value = -$root.input_value
      }
    }
  },

  four($root){
    return () => { $root.input_value = 4 }
  },

  append_zero($root){ return append_number($root, "0") },
  append_one($root){ return append_number($root, "1") },
  append_two($root){ return append_number($root, "2") },
  append_three($root){ return append_number($root, "3") },
  append_four($root){ return append_number($root, "4") },
  append_five($root){ return append_number($root, "5") },
  append_six($root){ return append_number($root, "6") },
  append_seven($root){ return append_number($root, "7") },
  append_eight($root){ return append_number($root, "8") },
  append_nine($root){ return append_number($root, "9") },

  append_decimal($root){ return append_number($root, ".")},

  operation_add($root){
    return bin_operation($root, (v1, v2) => v1 + v2)
  },

  operation_subtract($root){
    return bin_operation($root, (v1, v2) => v1 - v2)
  },

  operation_multiply($root){
    return bin_operation($root, (v1, v2) => v1 * v2)
  },

  operation_divide($root){
    return bin_operation($root, (v1, v2) => v1 / v2)
  },

  operation_sqrt($root){
    return un_operation($root, "sqrt")
  },

  operation_square($root){
    return un_operation($root, "square")
  },

  operation_sin($root){
    return un_operation($root, "sin")
  },

  operation_cos($root){
    return un_operation($root, "cos")
  },

  operation_tan($root){
    return un_operation($root, "tan")
  },

  input_pi($root){
    return () => {
      $root.input = null
      $root.last_value = Math.PI
      $root.operation = "equals"
    }
  },

  operation_equals($root){
    return () => {
      enter($root)
    }
  },

  clear($root){
    return () => {
      $root.input = null
    }
  },

  all_clear($root){
    return () => {
      $root.input = null
      $root.last_value = 0
      $root.operation = ""
    }
  },

  backspace($root){
    return () => {
      if($root.input != null){
        $root.input = $root.input.slice(0, -1)
      }
    }
  }
}