function getVideoURL(video){
  return "videos/" + video
}

function getPreviewURL(video){
  return "videos/720p/" + video
}

let videos = [
  "GOPR8953.MP4",
  "GOPR8954.MP4",
  "GOPR8955.MP4",
  "GOPR8956.MP4",
  "GOPR8957.MP4",
  "GOPR8958.MP4",
  "GOPR8959.MP4",
  "GOPR8960.MP4",
  "GOPR8962.MP4",
  "GOPR8963.MP4",
]

let videoSelect = <select class="video-select"></select>

for(let video of videos){
  videoSelect.appendChild(<option value={video}>{video}</option>)
}

let videoPlayer = <video class="video" controls preload="auto"></video>

let downloadLink = <a>Download</a>

function setActiveVideo(video){
  videoPlayer.src = getPreviewURL(video)
  downloadLink.href = getVideoURL(video)
  downloadLink.download = video
}

setActiveVideo(videos[0])

videoSelect.addEventListener("change", () => {
  setActiveVideo(videoSelect.value)
})

document.getElementById("output").appendChild(<div>
  <div class="control-container">
    <span>Video File: </span>
    {videoSelect}
    {downloadLink}
  </div>
  {videoPlayer}
</div>)



