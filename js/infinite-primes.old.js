window.appvars = {}

var container = document.getElementById("prime-container")

appvars.next_check = 2
appvars.prime_list = []

function next_prime(){

  let current_num = appvars.next_check++
  let current_num_root = Math.sqrt(current_num)

  for(var i = 0; i < appvars.prime_list.length; i++){
    let current_check = appvars.prime_list[i]

    if(current_num % current_check == 0){
      return next_prime()
    }
  }

  appvars.prime_list.push(current_num)
  return current_num

}

function create_el(){
  var el = document.createElement("p")

  el.innerHTML = next_prime()
  container.appendChild(el)

  return el
}

var last_el = create_el()

function test_el(){
  if(last_el.getBoundingClientRect().top < (window.innerHeight * 1.5)){
    last_el = create_el()
    test_el()
  }
}

test_el()

document.onscroll = function(e){
  test_el()
}
