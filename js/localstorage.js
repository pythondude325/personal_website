function loadFromStorage(){
  var stored_string = localStorage.getItem('save_data'); // The localStorage only stores strings at keys (save_data).
  var save_data = JSON.parse(stored_string); // The string is turned into a javascript object.
  $('#input').val(save_data.input_text); // Input's value is set to save_data.input_text.
  $('#status_label').text("localStorage Data Loaded.");
}

function saveToStorage(){
  var save_data = {}; // new object
  save_data.input_text = $('#input').val(); // save_data.input_text is set to the input's value.
  var data_string =  JSON.stringify(save_data); // The object is turned into a string because localStorage only stores strings.
  localStorage.setItem('save_data', data_string); // And saved to localStorage.
  $('#status_label').text("Data Saved to localStorage.")
}

function deleteStorage(){
  localStorage.removeItem('save_data'); // remove data
  $('#status_label').text("Data deleted from localStorage.");
}