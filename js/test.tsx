/* exported template */
let template: (title: string, body: string) => JSX.Element = (title: string, body: string): JSX.Element => <>
  <h3>{title}</h3>
  <p>{body}</p>
</>