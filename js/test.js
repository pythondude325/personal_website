/* global React */
/* exported template */
let template = (title, body) => React.createElement(React.Fragment, null, React.createElement("h3", null, title), React.createElement("p", null, body));