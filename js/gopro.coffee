export default (videos, output) ->
  videoSelector = <select class="video-select">
    {<option value={video}>{video}</option> for video in videos}
  </select>

  videoPlayer = <video class="video" controls preload="auto"></video>

  downloadLink = <a>Download</a>

  setActiveVideo = (video) ->
    videoPlayer.src = "videos/720p/" + video
    downloadLink.href = "videos/" + video
    downloadLink.download = video

  setActiveVideo videos[0]

  videoSelector.addEventListener "change", (e) ->
    setActiveVideo e.target.value

  output.appendChild <div>
    <div class="control-container">
      <span>Video File: </span>
      {videoSelector}
      {downloadLink}
    </div>
    {videoPlayer}
  </div>