
function toHexByte(num){
  return _.pad(_.clamp(num*256, 0, 255).toString(16), 2, "0");
}

var app = new Vue({
  el: "#app",
  data(){
    return {
      color: [1.0, 0.0, 0.0],
    }
  },
  computed: {
    hexColorCode(){
      return "#" + this.hexRed + this.hexGreen + this.hexBlue;
    },
    hexRed: {
      get(){ return toHexByte(this.color[0]); },
      set(v){ this.color[0] = parseInt(16)/256; }
    },
    hexGreen: {
      get(){ return toHexByte(this.color[1]); },
      set(v){ this.color[1] = parseInt(16)/256; }
    },
    hexBlue: {
      get(){ return toHexByte(this.color[2]); },
      set(v){ this.color[2] = parseInt(16)/256; }
    }
  }
});